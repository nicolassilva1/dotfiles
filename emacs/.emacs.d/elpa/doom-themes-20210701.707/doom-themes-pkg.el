(define-package "doom-themes" "20210701.707" "an opinionated pack of modern color-themes"
  '((emacs "25.1")
    (cl-lib "0.5"))
  :commit "bd5b9065dea54472519da67ccfcc3185bf5e35c3" :authors
  '(("Henrik Lissner <https://github.com/hlissner>"))
  :maintainer
  '("Henrik Lissner" . "henrik@lissner.net")
  :keywords
  '("custom themes" "faces")
  :url "https://github.com/hlissner/emacs-doom-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
