(require 'package)
(add-to-list 'package-archives
'("melpa" . "https://melpa.org/packages/"))
(package-refresh-contents)
(package-initialize)

(unless (package-installed-p 'use-package)
(package-install 'use-package))

(use-package general
:ensure t
:config
(general-evil-setup t))

(nvmap :prefix "SPC"
"SPC" '(counsel-M-x :which-key "M-x")
"." '(find-file :which-key "Find file")

;;Buffers
"b b" '(ibuffer :which-key "Ibuffer")
"b k" '(kill-current-buffer :which-key "Kill Current Buffer")
"b n" '(next-buffer :which-key "Next buffer")
"b p" '(previous-buffer :which-key "Previous Buffer")
"b B" '(ibuffer-list-buffers :which-key "Ibuffer list buffers")
"b K" '(kill-buffer :which-key "Kill buffer")
;;E-shell
"e h" '(counsel-esh-history :which-key "Eshell history")
"e s" '(eshell :which-key "Eshell")
"f r" '(counsel-recentf :which-key "Recent files")
"h r r" '((lambda () (interactive) (load-file "~/.emacs.d/init.el")) :which-key "Reload emacs config")
"t t" '(toggle-truncate-lines :which-key "Toggle truncate lines")
;; Window splits
"w c" '(evil-window-delete :which-key "Close window")
"w n" '(evil-window-new :which-key "New Window")
"w s" '(evil-window-split :which-key "Horizontal split window")
"w v" '(evil-window-vsplit :which-key "Vertical split window")
;; Window Motions
"w h" '(evil-window-left :which-key "Window left")
"w j" '(evil-window-down :which-key "Window down")
"w k" '(evil-window-up :which-key "Window updown")
"w l" '(evil-window-right :which-key "window right")
"w w" '(evil-window-next :which-key "Goto next window")


;;Closing
)

(use-package dashboard
:ensure t ;; install if not existent
:init ;;twak before loading
(setq dashboard-set-heading-icons t)
(setq dashboard-set-file-icons t)
(setq dashboard-banner-logo-title "Emacs Nico Configuration")
(setq dashboard-startup-banner 'logo) ;;default banner
(setq dashboard-center-content nil) ;;set to t for centerted content
(setq dashboard-items '((recents . 10) (agenda . 5) (bookmarks . 5) (projects . 5) (registers . 5)))
:config
(dashboard-setup-startup-hook)
(dashboard-modify-heading-icons '((recents . "file-text") (bookmarks . "book"))))

(use-package haskell-mode)

(use-package projectile
:ensure t
:config
(projectile-global-mode 1))

(use-package magit
:ensure t
:init
(message "Loading magit!!")
:config
(message "Loaded Magit!"))

(use-package evil
:ensure t ;;install it if not available
:init     ;; tweak evil before loading it
(setq evil-want-integration t) 
(setq evil-want-keybinding nil)
(setq evil-vsplit-window-right t)
(setq evil-split-window-below t)
(evil-mode))

(use-package evil-collection
:after evil
:ensure t
:config
(evil-collection-init))

(use-package doom-themes
:ensure t)
(setq doom-themes-enable-bold t
doom-themes-enable-italic t)
(load-theme 'doom-one t)

(use-package doom-modeline
:ensure t)
(doom-modeline-mode 1)

(set-face-attribute 'default nil
:font "SauceCodePro Nerd Font 11"
:weight 'medium)

(set-face-attribute 'variable-pitch nil
:font "Ubuntu Nerd Font 11"
:weight 'medium)


(set-face-attribute 'fixed-pitch nil
:font "SauceCodePro Nerd Font 11"
:weight 'medium)

;;Needed if using emacs as a client
(add-to-list 'default-frame-alist '(font . "SauceCodePro Nerd Font 11"))

(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-decrease)

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(use-package which-key
:ensure t)
(which-key-mode)
